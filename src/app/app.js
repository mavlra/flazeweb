var routerApp = angular.module('routerApp', ['ui.router']);

routerApp.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/planning');   
    $stateProvider
        
		.state('sidenavigator', {
            url: '/sidenavigator',
            templateUrl: 'app/views/sidenavigator.html'
        })

        .state('planning', {
            url: '/planning',
            templateUrl: 'app/views/planning-home.html'
        })
        
		.state('finance', {
            url: '/finance',
            templateUrl: 'app/views/finance-home.html'
        })
		
        .state('dashboard', {
			url: '/dashboard',
			views: {

				'': { templateUrl: 'app/views/partials/partial-dashboard.html' },

				'columnOne@dashboard': { template: 'Graph I!' },

				'columnTwo@dashboard': {
					templateUrl: 'app/views/table-data.html',
					controller: 'scotchController'
				},
				
				'columnThree@dashboard': { template: 'Graph III!' },
				
				'columnFour@dashboard': { template: 'Graph IV!' }
			}	
		})

		.state('planning.demand', {
			url: '/demand',
			templateUrl: 'demand-list.html',
			controller: function($scope) {
				$scope.dogs = ['Bernese', 'Husky', 'Goldendoodle'];
			}
		})

		.state('planning.supply', {
			url: '/supply',
			template: 'It is Supply view.'
		})

		.state('planning.supplydemand', {
			url: '/supplydemand',
			template: 'It is Supply & Demand view.'
		})
});

routerApp.controller('scotchController', function($scope) {
    $scope.message = 'test';   
    $scope.scotches = [
        {
            name: 'Macallan 12',
            price: 50
        },
        {
            name: 'Chivas Regal Royal Salute',
            price: 10000
        },
        {
            name: 'Glenfiddich 1937',
            price: 20000
        }
    ];
});