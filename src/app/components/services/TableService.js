  (function(){
  'use strict';

  angular.module('app')
        .service('tableService', /*[
        '$q',
      tableService
  ]*/   function( $http, $q ) {
          // Return public API.
          return ({
            getUsers: getUsers,
          });
          function getUsers() {
            var headers = {
              'Access-Control-Allow-Origin' : '*',
              'Access-Control-Allow-Methods' : 'GET,POST,PUT,DELETE,OPTIONS',
              'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            };
            var request = $http({
                method: "get",
              //url: "http://mavlracorp.ddns.net:9090/Flaze/rest/UserService/users",
                url: "http://localhost:9090/Flaze/rest/UserService/users",
                //headers: headers,
                  /*params: {
                    action: "get"
                  }*/
            });
            return ( request.then(handleSuccess, handleError) );
          }

          // ---
          // PRIVATE METHODS.
          // ---

          // I transform the error response, unwrapping the application dta from
          // the API response payload.
          function handleError(response) {
            // The API response from the server should be returned in a
            // nomralized format. However, if the request was not handled by the
            // server (or what not handles properly - ex. server error), then we
            // may have to normalize it on our end, as best we can.
            if (
                !angular.isObject(response.data) || !response.data.message
            ) {
              return ( $q.reject("An unknown error occurred.") );
            }
            // Otherwise, use expected error message.
            return ( $q.reject(response.data.message) );
          }

          // I transform the successful response, unwrapping the application data
          // from the API response payload.
          function handleSuccess( response ) {
             return( response.data );
         }
        });
    })();
          /*
          function tableService($q, $http){
            var tabledata;

              return {
                  loadAllItems : function() {
                    $http.get('http://mavlracorp.ddns.net:9090/Flaze/rest/UserService/users').
                    success(function(data) {
                    return tabledata;
                    }
                    );
                  }
              };
           }
           })();
          */

/*function tableService($http, $q) {
  var text = $q;
  var http = $http;
  var tabledata = [];
  return {
    loadAllItems: function ($q) {
      $http.get('http://mavlracorp.ddns.net:9090/Flaze/rest/UserService/users').success(function (data) {
        tabledata = data;
        console.log(tabledata);
        return $q.when(tableData);
      });
    }
  }
}
})();*/


  /*function tableService($q){
    var tableData = [
      {
        issue: 'Nested views',
        progress: 100,
        status: 'Done',
        class: 'md-accent'
      },
      {
        issue: 'Table component',
        progress: 40,
        status: 'Feedback',
        class: ''
      },
      {
        issue: 'Dashboard tiles',
        progress: 100,
        status: 'Done',
        class: 'md-accent'
      },
      {
        issue: 'Panel widget',
        progress: 84,
        status: 'In progress',
        class: 'orange'
      },
      {
        issue: 'Form',
        progress: 100,
        status: 'Done',
        class: 'md-accent'
      },
      {
        issue: 'Custom CSS',
        progress: 20,
        status: 'Feedback',
        class: ''
      },
      {
        issue: 'Add backend',
        progress: 1,
        status: 'To do',
        class: 'md-warn'
      },
      {
        issue: 'Layout with sidebar',
        progress: 100,
        status: 'Done',
        class: 'md-accent'
      }
    ];

    return {
      loadAllItems : function() {
        return $q.when(tableData);
      }
    };
  }
})();*/
