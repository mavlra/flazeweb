(function(){

  angular
    .module('app')
    .controller('TableController', [
      'tableService',
      TableController
    ]);

  function TableController(tableService) {
    var vm = this;

    vm.tableData = [];

    tableService
      /*.loadAllItems()*/
        .getUsers()
      .then(function(tableData) {
        vm.tableData = [].concat(tableData);
      });
  }

})();
